package controllers

import (
	"example/hello/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UpdateHolidayInput struct {
	Date  string `json:"date"`
	Title string `json:"title"`
}

func UpdateHoliday(c *gin.Context) {

	var holidays models.Holiday
	if err := models.DB.Where("id = ?", c.Param("id")).First(&holidays).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input UpdateHolidayInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&holidays).Updates(input)

	c.JSON(http.StatusOK, gin.H{"data": holidays})
}
