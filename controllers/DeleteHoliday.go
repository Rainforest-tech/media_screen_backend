package controllers

import (
	"example/hello/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func DeleteHoliday(c *gin.Context) {
	// Get model if exist
	var holidays models.Holiday
	if err := models.DB.Where("id = ?", c.Param("id")).First(&holidays).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&holidays)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
