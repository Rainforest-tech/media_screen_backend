package controllers

import (
	"example/hello/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type CreateHolidayInput struct {
	Date  string `json:"date" binding:"required"`
	Title string `json:"title" binding:"required"`
}

func CreateHoliday(c *gin.Context) {

	var input CreateHolidayInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	holidays := models.Holiday{Date: input.Date, Title: input.Title}
	models.DB.Create(&holidays)

	c.JSON(http.StatusOK, gin.H{"data": holidays})
}
