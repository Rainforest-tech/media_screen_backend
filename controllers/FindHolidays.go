package controllers

import (
	"example/hello/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func FindHolidays(c *gin.Context) {
	var holidays []models.Holiday
	models.DB.Find(&holidays)

	c.JSON(http.StatusOK, gin.H{"data": holidays})
}
