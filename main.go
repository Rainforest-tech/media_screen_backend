package main

import (
	"example/hello/controllers"
	"example/hello/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "any"})
	})

	models.ConnectDatabase()
	r.GET("/holidays", controllers.FindHolidays)
	r.POST("/holidays", controllers.CreateHoliday)
	r.GET("/holidays/:id", controllers.FindOneHoliday)
	r.PATCH("/holidays/:id", controllers.UpdateHoliday)
	r.DELETE("/books/:id", controllers.DeleteHoliday)

	r.Run()
}
