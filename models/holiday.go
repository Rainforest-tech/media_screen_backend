package models

type Holiday struct {
	ID    uint   `json:"id" gorm:"primary_key"`
	Date  string `json:"date"`
	Title string `json:"title"`
}
